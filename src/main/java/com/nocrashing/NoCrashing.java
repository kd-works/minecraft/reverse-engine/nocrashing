package com.nocrashing;

import com.nocrashing.antiflood.AntiFlood;
import com.nocrashing.antiflood.AntiFloodTask;
import com.nocrashing.author.Yanis6660;
import com.nocrashing.controller.Controller;
import com.nocrashing.disconnect.BookMessage;
import com.nocrashing.disconnect.DisconnectManager;
import com.nocrashing.disconnect.KickTask;
import com.nocrashing.packets.PacketInjector;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Iterator;

public class NoCrashing extends JavaPlugin {
    private final DisconnectManager disconnectManager = new DisconnectManager(this);
    private final AntiFlood antiFlood = new AntiFlood();
    private final BookMessage bookMessage = new BookMessage(this);

    public void onEnable() {
        new Yanis6660();
        new Controller(this);
        new KickTask(this.disconnectManager);
        new AntiFloodTask(this.antiFlood);

        Iterator iterator = NoCrashing.Aw("g3iav0").iterator();

        while (iterator.hasNext()) {
            Player player = (Player)iterator.next();
            NoCrashing.Aw("-1c7bl0n", (Object)new PacketInjector(player));
            NoCrashing.Aw("-1du3l0t", (Object)this.bookMessage, (Object)player.getName());
        }
    }

    public void onDisable() {
        Iterator iterator = NoCrashing.Aw("g3iav0").iterator();

        while (iterator.hasNext()) {
            Player player = (Player)iterator.next();
            NoCrashing.Aw("-1e8jl0s", (Object)new PacketInjector(player));
        }
    }

    public DisconnectManager getDisconnectManager() {
        return this.disconnectManager;
    }

    public AntiFlood getAntiFlood() {
        return this.antiFlood;
    }

    public BookMessage getBookMessage() {
        return this.bookMessage;
    }
}
