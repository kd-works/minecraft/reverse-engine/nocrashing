package com.nocrashing.packets;

import com.nocrashing.NoCrashing;
import com.nocrashing.controller.type.MineListener;
import com.nocrashing.packets.PacketInjector;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class Packet extends MineListener {
    public Packet(NoCrashing noCrashing) {
        super(noCrashing);
    }

    @EventHandler
    public void playerJoin(PlayerJoinEvent playerJoinEvent) {
        Object object = Packet.vr("1aa177q", (Object)playerJoinEvent);
        Packet.vr("3db77d", (Object)new PacketInjector((Player)object));
        Packet.vr("5ll77f", (Object)Packet.vr("1ee177c", (Object)this.getMain()), (Object)object.getName());
    }

    @EventHandler
    public void playerLeave(PlayerQuitEvent playerQuitEvent) {
        Object object = Packet.vr("1i7p77e", (Object)playerQuitEvent);
        Packet.vr("1ppb77h", (Object)new PacketInjector((Player)object));
    }
}
