package com.nocrashing.packets;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class ReadPacket extends PlayerEvent implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private Object packet;

    public ReadPacket(Player player, Object object) {
        super(player);
        this.packet = object;
    }

    public Object getPacket() {
        return this.packet;
    }

    public String getPacketName() {
        return ReadPacket.Rx("i6n715", (Object)ReadPacket.Rx("145h77i", (Object)this.packet));
    }

    public boolean isCancelled() {
        return this.cancelled;
    }

    public void setCancelled(boolean bl) {
        this.cancelled = bl;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
