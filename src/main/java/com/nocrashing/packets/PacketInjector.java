package com.nocrashing.packets;

import com.nocrashing.packets.PacketListener;
import com.nocrashing.resolver.Reflection;

import org.bukkit.entity.Player;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelPipeline;

public class PacketInjector {
    private final Player player;
    private final String channel_name;
    private final Reflection.MethodInvoker getPlayerHandle = PacketInjector.IW("1abr77u", (Object)"{obc}.entity.CraftPlayer", (Object)"getHandle", (Class[])new Class[0]);
    private final Reflection.FieldAccessor<Object> getConnection = PacketInjector.IW("1ulp781", (Object)"{nms}.EntityPlayer", (Object)"playerConnection", Object.class);
    private final Reflection.FieldAccessor<Object> getManager = PacketInjector.IW("1ulp781", (Object)"{nms}.PlayerConnection", (Object)"networkManager", Object.class);
    private final Reflection.FieldAccessor<Channel> getChannel = PacketInjector.IW("-1l3qoo0", (Object)"{nms}.NetworkManager", Channel.class, (int)0);

    public PacketInjector(Player player) {
        this.player = player;
        this.channel_name = "NoCrashing_Channel";
    }

    public void injectChannel() {
        ChannelPipeline channelPipeline = this.getChannel(this.player).pipeline();
        channelPipeline.addBefore("packet_handler", this.channel_name, new PacketListener(this.player));
    }

    public void removeChannel() {
        Channel channel = this.getChannel(this.player);
        channel.eventLoop().submit(() -> channel.pipeline().remove(this.channel_name));
    }

    public Channel getChannel(Player player) {
        Object object = this.getConnection.get(this.getPlayerHandle.invoke(player, new Object[0]));
        Object object2 = this.getManager.get(object);
        return this.getChannel.get(object2);
    }
}
