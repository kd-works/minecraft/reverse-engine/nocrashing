package com.nocrashing.disconnect;

public final class TypeKick extends Enum<TypeKick> {
    public static final /* enum */ TypeKick BLOCK_EXPLOIT = new TypeKick();
    public static final /* enum */ TypeKick BOOK_EXPLOIT = new TypeKick();
    public static final /* enum */ TypeKick CREATIVE_EXPLOIT = new TypeKick();
    public static final /* enum */ TypeKick SIGN_EXPLOIT = new TypeKick();
    public static final /* enum */ TypeKick WINDOWS_CLICK_EXPLOIT = new TypeKick();
    public static final /* enum */ TypeKick CHANNEL_EXPLOIT = new TypeKick();
    private static final /* synthetic */ TypeKick[] ENUM$VALUES;

    static {
        TypeKick[] arrtypeKick = new TypeKick[6];
        arrtypeKick[0] = BLOCK_EXPLOIT;
        arrtypeKick[1] = BOOK_EXPLOIT;
        arrtypeKick[2] = CREATIVE_EXPLOIT;
        arrtypeKick[3] = SIGN_EXPLOIT;
        arrtypeKick[4] = WINDOWS_CLICK_EXPLOIT;
        arrtypeKick[5] = CHANNEL_EXPLOIT;
        ENUM$VALUES = arrtypeKick;
    }

    public static TypeKick[] values() {
        TypeKick[] arrtypeKick = ENUM$VALUES;
        int n = arrtypeKick.length;
        TypeKick[] arrtypeKick2 = new TypeKick[n];
        TypeKick.ti("-pj361k", (Object)arrtypeKick, (int)0, (Object)arrtypeKick2, (int)0, (int)n);
        return arrtypeKick2;
    }

    public static TypeKick valueOf(String string) {
        return (TypeKick)((Object)TypeKick.ti("1cekpud", TypeKick.class, (Object)string));
    }
}
