package com.nocrashing.disconnect;

import com.nocrashing.NoCrashing;
import com.nocrashing.disconnect.TypeKick;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;

import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;
import net.minecraft.server.v1_8_R3.NBTTagString;
import net.minecraft.server.v1_8_R3.PacketDataSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutCustomPayload;

import io.netty.buffer.ByteBuf;

import java.util.concurrent.ConcurrentHashMap;

public class BookMessage {
    private final ConcurrentHashMap<String, String> crashmessage;
    private final NoCrashing main;

    public BookMessage(NoCrashing noCrashing) {
        this.main = noCrashing;
        this.crashmessage = new ConcurrentHashMap<>();
    }

    public void addMessage(String string, TypeKick typeKick) {
        if (BookMessage.CJ("nvipti", this.crashmessage, (Object)string) != false) {
            BookMessage.CJ("-1q8j62c", this.crashmessage, (Object)string);
        }

        BookMessage.CJ("1o6sptj", this.crashmessage, (Object)string, (Object)BookMessage.CJ("10ekpsl", (Object)BookMessage.CJ("13uapsn", (Object)BookMessage.CJ("13uapsn", (Object)new StringBuilder("\u00a74  \u00a7nTentative de Crash\u00a7c \n\nNous vous avons \u00e9ject\u00e9 du serveur car vous \u00eates soup\u00e7onn\u00e9 d'avoir tent\u00e9: \u00a74"), (Object)BookMessage.CJ("-iif63c", (Object)typeKick)), (Object)"\u00a7c. \n\n\u00a74\u00a7nCe type d'attaque n'est pas tol\u00e9r\u00e9e !")));
    }

    public ConcurrentHashMap<String, String> getMessage() {
        return this.crashmessage;
    }

    public void sendCrashMessage(final String string) {
        if (BookMessage.CJ("nvipti", this.crashmessage, (Object)string) != false && BookMessage.CJ("-193n63e", (Object)string) != null) {
            BookMessage.CJ("-jhf62f", (Object)new BukkitRunnable(){
                public void run() {
                    try {
                        _1.ZT("-k3962o", (long)1000L);
                    } catch (InterruptedException interruptedException) {
                        _1.ZT("-1cm5627", (Object)interruptedException);
                    }
                    String[] arrstring = new String[1];
                    arrstring[0] = (String)_1.ZT("-nh361n", (Object)_1.ZT("-r2h61l", (Object)BookMessage.this), (Object)string);
                    _1.ZT("-8if629", (Object)BookMessage.this, (Object)_1.ZT("a0optm", (Object)BookMessage.this, (Object)"Tentive de Crash", (Object)"Yanis6660", (String[])arrstring), (Object)_1.ZT("-193n63e", (Object)string));
                    _1.ZT("-1q8j62c", (Object)_1.ZT("-r2h61l", (Object)BookMessage.this), (Object)string);
                }
            }, (Object)this.main);
        }
    }

    private void openBook(ItemStack itemStack, Player player) {
        int n = player.getInventory().getHeldItemSlot();
        ItemStack itemStack2 = player.getInventory().getItem(n);
        player.getInventory().setItem(n, itemStack);
        Object object = BookMessage.CJ("a5gpuu", (int)256);
        BookMessage.CJ("1ee4puv", (Object)object, (int)0, (int)0);
        BookMessage.CJ("1kc4pus", (Object)object, (int)1);
        PacketPlayOutCustomPayload packetPlayOutCustomPayload = new PacketPlayOutCustomPayload("MC|BOpen", new PacketDataSerializer((ByteBuf)object));
        BookMessage.CJ("1866put", (Object)((EntityPlayer)BookMessage.CJ("-3tf624", (Object)player)).playerConnection, (Object)packetPlayOutCustomPayload);
        player.getInventory().setItem(n, itemStack2);
    }

    private ItemStack book(String string, String string2, String ... arrstring) {
        ItemStack itemStack = new ItemStack(Material.WRITTEN_BOOK, 1);
        Object object = BookMessage.CJ("-1qv9615", (Object)itemStack);
        NBTTagCompound nBTTagCompound = new NBTTagCompound();
        BookMessage.CJ("eckpu8", (Object)nBTTagCompound, (Object)"title", (Object)string);
        BookMessage.CJ("eckpu8", (Object)nBTTagCompound, (Object)"author", (Object)string2);
        NBTTagList nBTTagList = new NBTTagList();
        String[] arrstring2 = arrstring;
        int n = arrstring.length;

        for (int i = 0; i < n; ++i) {
            String string3 = arrstring2[i];
            if (BookMessage.CJ("-1g01617", (Object)string3) != false) continue;
            BookMessage.CJ("-12hr61q", (Object)nBTTagList, (Object)new NBTTagString(string3));
        }

        BookMessage.CJ("-1per61p", (Object)nBTTagCompound, (Object)"pages", (Object)nBTTagList);
        BookMessage.CJ("-bth61s", (Object)object, (Object)nBTTagCompound);
        return BookMessage.CJ("f2pu5", (Object)object);
    }

    static /* synthetic */ ConcurrentHashMap access$0(BookMessage bookMessage) {
        return bookMessage.crashmessage;
    }

    static /* synthetic */ ItemStack access$1(BookMessage bookMessage, String string, String string2, String[] arrstring) {
        return bookMessage.book(string, string2, arrstring);
    }

    static /* synthetic */ void access$2(BookMessage bookMessage, ItemStack itemStack, Player player) {
        bookMessage.openBook(itemStack, player);
    }
}
