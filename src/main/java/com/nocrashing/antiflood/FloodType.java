package com.nocrashing.antiflood;

public final class FloodType extends Enum<FloodType> {
    public static final /* enum */ FloodType BLOCK_FLOOD = new FloodType();
    public static final /* enum */ FloodType WINDOWS_CLICK_FLOOD = new FloodType();
    private static final /* synthetic */ FloodType[] ENUM$VALUES;

    static {
        FloodType[] arrfloodType = new FloodType[2];
        arrfloodType[0] = BLOCK_FLOOD;
        arrfloodType[1] = WINDOWS_CLICK_FLOOD;
        ENUM$VALUES = arrfloodType;
    }

    public static FloodType[] values() {
        FloodType[] arrfloodType = ENUM$VALUES;
        int n = arrfloodType.length;
        FloodType[] arrfloodType2 = new FloodType[n];
        FloodType.Lp("1h9d40i", (Object)arrfloodType, (int)0, (Object)arrfloodType2, (int)0, (int)n);
        return arrfloodType2;
    }

    public static FloodType valueOf(String string) {
        return (FloodType)((Object)FloodType.Lp("1h540j", FloodType.class, (Object)string));
    }
}
