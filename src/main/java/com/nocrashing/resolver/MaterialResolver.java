package com.nocrashing.resolver;

public final class MaterialResolver extends Enum<MaterialResolver> {
    public static final /* enum */ MaterialResolver BOOK = new MaterialResolver();
    public static final /* enum */ MaterialResolver WRITING_BOOK = new MaterialResolver();
    public static final /* enum */ MaterialResolver WRITTEN_BOOK = new MaterialResolver();
    private static final /* synthetic */ MaterialResolver[] ENUM$VALUES;

    static {
        MaterialResolver[] arrmaterialResolver = new MaterialResolver[3];
        arrmaterialResolver[0] = BOOK;
        arrmaterialResolver[1] = WRITING_BOOK;
        arrmaterialResolver[2] = WRITTEN_BOOK;
        ENUM$VALUES = arrmaterialResolver;
    }

    public static MaterialResolver[] values() {
        MaterialResolver[] arrmaterialResolver = ENUM$VALUES;
        int n = arrmaterialResolver.length;
        MaterialResolver[] arrmaterialResolver2 = new MaterialResolver[n];
        MaterialResolver.Ab("g3i89l", (Object)arrmaterialResolver, (int)0, (Object)arrmaterialResolver2, (int)0, (int)n);
        return arrmaterialResolver2;
    }

    public static MaterialResolver valueOf(String string) {
        return (MaterialResolver)((Object)MaterialResolver.Ab("6dm882", MaterialResolver.class, (Object)string));
    }
}
