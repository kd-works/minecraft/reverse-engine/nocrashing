package com.nocrashing.resolver;

import org.bukkit.Material;
import org.bukkit.inventory.meta.BookMeta;

import net.minecraft.server.v1_8_R3.ItemStack;

import java.util.Iterator;

public class NbtResolver {
    private final org.bukkit.inventory.ItemStack itemstack;

    public NbtResolver(ItemStack itemStack) {
        this.itemstack = NbtResolver.XS("1g2k87e", (Object)itemStack);
    }

    public NbtResolver(org.bukkit.inventory.ItemStack itemStack) {
        this.itemstack = itemStack;
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public boolean check() {
        if (this.itemstack == null) return false;
        if (NbtResolver.XS("g0e89c", (Object)this.itemstack) == false) return false;
        if (NbtResolver.XS("-8fdnmj", (Object)this.itemstack) == Material.BOOK) {
            if (NbtResolver.XS("g0e89c", (Object)this.itemstack) != false) {
                if (NbtResolver.XS("-v05nm5", (Object)this.itemstack).hasDisplayName()) {
                    return false;
                }
            }
        }

        try {
            String string;
            BookMeta bookMeta = (BookMeta)NbtResolver.XS("-v05nm5", (Object)this.itemstack);
            if (bookMeta == null) return false;
            if (!bookMeta.hasPages()) return false;

            if (bookMeta.getPageCount() > 50) {
                return true;
            }

            Iterator iterator = bookMeta.getPages().iterator();

            do {
                if (iterator.hasNext()) continue;
                return false;
            } while (NbtResolver.XS("-15vtnm8", (Object)(string = (String)iterator.next())) <= 256);

            return true;
        } catch (Exception exception) {
            return false;
        }
    }
}
