package com.nocrashing.controller.extend;

import com.nocrashing.NoCrashing;

public class MyController {
    private final NoCrashing main;

    public MyController(NoCrashing noCrashing) {
        this.main = noCrashing;
    }

    public final NoCrashing getMain() {
        return this.main;
    }
}
