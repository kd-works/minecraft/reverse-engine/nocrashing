package com.nocrashing.controller.type;

import com.nocrashing.NoCrashing;
import com.nocrashing.controller.extend.MyController;

import org.bukkit.scheduler.BukkitScheduler;

public abstract class MineTask extends MyController implements Runnable {
    public MineTask(NoCrashing noCrashing) {
        super(noCrashing);
    }

    public void addTask(int n, int n2) {
        BukkitScheduler bukkitScheduler = MineTask.mo("-ovq45h", (Object)this.getMain()).getScheduler();
        bukkitScheduler.runTaskTimer(this.getMain(), this, n, n2);
    }

    public void addTaskAsyncRepeating(int n, int n2) {
        BukkitScheduler bukkitScheduler = MineTask.mo("-ovq45h", (Object)this.getMain()).getScheduler();
        bukkitScheduler.scheduleAsyncRepeatingTask(this.getMain(), this, n, n2);
    }

    public void addTask(long l, int n) {
        BukkitScheduler bukkitScheduler = MineTask.mo("-ovq45h", (Object)this.getMain()).getScheduler();
        bukkitScheduler.runTaskTimer(this.getMain(), this, l, n);
    }
}
